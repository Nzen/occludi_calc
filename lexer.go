package calc

import (
	"fmt"
	"unicode"
	"unicode/utf8"
)

const (
	// NOTE t is for type
	T_L_PAREN = iota
	T_R_PAREN = iota
	T_PLUS = iota
	T_MINUS = iota
	T_MULTIPLY = iota
	T_DIVIDE = iota
	T_NUMBER = iota
	T_BLANK = iota
	T_END_LINE = iota
	T_END_FILE = iota
	T_START_FILE = iota
	T_UNKNOWN = iota
	// NOTE r is for rune
	r_lparen = rune( '(' )
	r_rparen = rune( ')' )
	r_plus = rune( '+' )
	r_hyphen = rune( '-' )
	r_asterisk = rune( '*' )
	r_ll_slash = rune( '/' )
	R_eof = rune( 1 )
	R_eol = rune( 2 )
	R_bof = rune( 3 )
)

// Lexer holds a stream of Lexemes and an awareness of the current Lexeme.
type Lexer struct {
	original string
	tokens []Lexeme
	cursor int
}

// Lex converts the string into Lexemes for later iteration.
func (receive *Lexer) Lex( input string ) error {
	/*
	get runes from input
	for rune
		if single char
			save lexeme
		else
			gather the relevant runes
			save lexeme of multiple runes
	*/
	receive.original = input
	receive.cursor = 0
	if ( len( input ) == 0 ) {
		receive.tokens = []Lexeme{
			Lexeme {
				Ltype : T_END_FILE,
				Word : []rune{ R_eof } } }
		return nil;
	} // else
	receive.tokens = make( []Lexeme, utf8.RuneCountInString( input ) )
		// NOTE sacrificing space above for simplicity, rather than precalculating the exact size or appending many times
	exploded := make( []rune, utf8.RuneCountInString( input ) )
	indE := 0
	for _, nibble := range input { // IMPROVE recall that ind is a byte index, which is dangerous for me
		exploded[ indE ] = nibble
		indE += 1
	}
	line := 0
	tokenInd := -1
	for cursor := 0; cursor < len( exploded ); cursor += 1 {
		tokenInd += 1
		char := exploded[ cursor ]
		// NOTE checking for single rune lexemes
		if char == r_lparen {
			receive.tokens[ tokenInd ] = Lexeme{ T_L_PAREN, line, cursor, []rune{ char } }
	fmt.Println( "a", tokenInd, cursor, TypeDesc( T_L_PAREN ) )
			continue
		} else if char == r_rparen {
			receive.tokens[ tokenInd ] = Lexeme{ T_R_PAREN, line, cursor, []rune{ char } }
	fmt.Println( "a", tokenInd, cursor, TypeDesc( T_R_PAREN ) )
			continue
		} else if char == r_ll_slash {
			receive.tokens[ tokenInd ] = Lexeme{ T_DIVIDE, line, cursor, []rune{ char } }
	fmt.Println( "a", tokenInd, cursor, TypeDesc( T_DIVIDE ) )
			continue
		} else if char == r_asterisk {
			receive.tokens[ tokenInd ] = Lexeme{ T_MULTIPLY, line, cursor, []rune{ char } }
	fmt.Println( "a", tokenInd, cursor, TypeDesc( T_MULTIPLY ) )
			continue
		} else if char == r_plus {
			receive.tokens[ tokenInd ] = Lexeme{ T_PLUS, line, cursor, []rune{ char } }
	fmt.Println( "a", tokenInd, cursor, TypeDesc( T_PLUS ) )
			continue
		} else if char == r_hyphen {
			receive.tokens[ tokenInd ] = Lexeme{ T_MINUS, line, cursor, []rune{ char } }
	fmt.Println( "a", tokenInd, cursor, TypeDesc( T_MINUS ) )
			continue
		}
		/* deactivated until I reread how switch works, given that it's, apparently not checking all of the cases
		switch char {
			case r_lparen :
				receive.tokens[ tokenInd ] = Lexeme{ T_L_PAREN, line, cursor, []rune{ char } }
				continue
			case r_rparen :
				receive.tokens[ tokenInd ] = Lexeme{ T_R_PAREN, line, cursor, []rune{ char } }
				continue
			case r_ll_slash :
				receive.tokens[ tokenInd ] = Lexeme{ T_DIVIDE, line, cursor, []rune{ char } }
				continue
			case r_asterisk :
				receive.tokens[ tokenInd ] = Lexeme{ T_MULTIPLY, line, cursor, []rune{ char } }
				continue
			case r_plus :
				receive.tokens[ tokenInd ] = Lexeme{ T_PLUS, line, cursor, []rune{ char } }
				continue
			case r_hyphen:
				receive.tokens[ tokenInd ] = Lexeme{ T_MINUS, line, cursor, []rune{ char } }
				continue
		}
		*/
		if isNewLine( char ) {
			if cursor +1 >= len( exploded ) {
				receive.tokens[ tokenInd ] = Lexeme{ T_END_FILE, line, cursor, []rune{ R_eof } }
				continue
			} // else more chars exist
			if isNewLine( exploded[ cursor +1 ] ) {
				receive.tokens[ tokenInd ] = Lexeme{ T_END_LINE, line, cursor, []rune{ char, exploded[ cursor +1 ] } }
				cursor += 1
			} else {
				receive.tokens[ tokenInd ] = Lexeme{ T_END_LINE, line, cursor, []rune{ R_eol } }
			}
			line += 1
			continue
		}
		var checker func( rune ) bool
		sequenceType := T_UNKNOWN
		if unicode.IsDigit( char ) {
			sequenceType = T_NUMBER
			checker = unicode.IsDigit
		} else if unicode.IsSpace( char ) {
			sequenceType = T_BLANK
			checker = unicode.IsSpace
		} else {
			receive.tokens[ tokenInd ] = Lexeme{ T_UNKNOWN, line, cursor, []rune{ char } }
			// IMPROVE get a run of unknown
			continue
		}
		start := cursor
		for ; checker( char ) && cursor < len( exploded ); cursor += 1 {
			char = exploded[ cursor ]
		}
		cursor -= 1 // NOTE roll back to a char that passes checker()
		// FIX "2/" => start:0 cursor:1 ;; so consider how subtracting 2 or setting cursor back or whatever is necessary to fix this so cursor starts in the right place
		chars := make( []rune, cursor - start )
		charsInd := 0
		for attention := start; attention < cursor; attention += 1 {
			chars[ charsInd ] = exploded[ attention ]
			charsInd += 1
			// ASK or chars[ ind - prev ] = exploded[ ind ]
		}
		cursor -= 1
	fmt.Println( "a", tokenInd, cursor, TypeDesc( sequenceType ) )
		receive.tokens[ tokenInd ] = Lexeme{ sequenceType, line, start, chars }
		continue
	}
	return nil
}

func (hasTokens *Lexer) NextToken() Lexeme {
	if hasTokens.cursor >= len( hasTokens.tokens ) {
		return Lexeme {
				Ltype : T_END_FILE,
				Word : []rune{ R_eof } }
	}
	requested := hasTokens.tokens[ hasTokens.cursor ]
	hasTokens.cursor += 1
	return requested
}

func (hasTokens *Lexer) PrevToken() Lexeme {
	if hasTokens.cursor < 0 {
		return Lexeme {
				Ltype : T_END_FILE,
				Word : []rune{ R_bof } }
	}
	requested := hasTokens.tokens[ hasTokens.cursor ]
	hasTokens.cursor -= 1
	return requested
}

func (hasTokens Lexer) Len() int {
	return len( hasTokens.tokens )
}

func isNewLine( char rune ) bool {
	return char == rune( '\n' ) ||
		char == rune( '\r' )
}

// add lexeme 'method' to get a string from the l.Word

/* Lexe retrieves the next Lexeme from the original string.
 Requesting more Lexemes after end of file will continue to produce the eof Lexeme. */
func Lexe( textIn string ) func() Lexeme {
	exploded := make( []rune, utf8.RuneCountInString( textIn ) )
	// IMPROVE these are byte, not rune indicies, a non ascii will mess this up
	for ind, nibble := range textIn {
		exploded[ ind ] = nibble
	}
	line := 0
	prev := 0
	cursor := 0
	return func() Lexeme {
		if cursor >= len( exploded ) -1 {
			return Lexeme{ T_END_FILE, line, cursor, []rune{ 0 } }
		}
fmt.Println( "cur-", cursor, " len-", len( exploded ) )
		prev = cursor
		char := exploded[ cursor ]
		// NOTE checking for single rune lexemes
		switch char {
			case r_lparen :
				cursor += 1
				return Lexeme{ T_L_PAREN, line, prev, []rune{ char } }
			case r_rparen :
				cursor += 1
				return Lexeme{ T_R_PAREN, line, prev, []rune{ char } }
			case r_ll_slash :
				cursor += 1
				return Lexeme{ T_DIVIDE, line, prev, []rune{ char } }
			case r_asterisk :
				cursor += 1
				return Lexeme{ T_MULTIPLY, line, prev, []rune{ char } }
			case r_plus :
				cursor += 1
				return Lexeme{ T_PLUS, line, prev, []rune{ char } }
			case r_hyphen:
				cursor += 1
				return Lexeme{ T_MINUS, line, prev, []rune{ char } }
		}
		// NOTE check for multi rune lexemes
		var checker func( rune ) bool;
		sequenceType := T_UNKNOWN;
		if unicode.IsDigit( char ) {
			sequenceType = T_NUMBER
			checker = unicode.IsDigit
		} else if unicode.IsSpace( char ) {
			sequenceType = T_BLANK
			checker = unicode.IsSpace
		} else {
			checker = nil
		}
		if checker == nil {
			// FIX get a run of non things-I-want
			return Lexeme{ T_UNKNOWN, line, prev, []rune{ -1 } }
		}
		for ; checker( char ) && cursor < len( exploded ) -1; cursor += 1 {
			char = exploded[ cursor ]
		}
		cursor -= 1 // NOTE roll back to a char that passes checker()
		// length := cursor - prev
		// IMPROVE just make a slice of the original by tracking indicies ?
		chars := make( []rune, cursor - prev )
		ind := 0
		// FIX this and the prev = cursor may push my cursor two past where I expect it maybe
		for attention := prev; attention < cursor; attention += 1 {
			chars[ ind ] = exploded[ attention ]
			ind += 1
			// ASK or chars[ ind - prev ] = exploded[ ind ]
		}
		return Lexeme{ sequenceType, line, prev, chars }
	}
}

/*
package main

import (
	"fmt"
	"calc"
)

func main() {
	rCalc()
}

func rCalc() {
	input :=  "2/ 0 *1 + 32-3"
	understand := calc.Lexer{}
	understand.Lex( input )
	for {
		piece := understand.NextToken()
		if piece.Ltype == calc.T_END_FILE {
			break
		}
		fmt.Println( piece )
	}
}
*/

























