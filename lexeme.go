
package calc // preferably nzen.ws/format/calc

import (
	"fmt"
)

type Lexeme struct {
	Ltype int
	LineInd int
	CharInd int
	Word []rune // NOTE handles lexemes of multiple chars
}

func ( token Lexeme ) String() string {
	lenOf := len( token.Word )
	if lenOf > 1 {
		// IMPROVE make a string from all the runes
		return fmt.Sprintf(
			"<ty-%s ind-%d val-%s>",
			TypeDesc( token.Ltype ),
			token.CharInd,
			string( token.Word[ 0 ] ) )
	} else if lenOf == 0 {
		return fmt.Sprintf(
			"<ty-%s ind-%d val-%s>",
			TypeDesc( token.Ltype ),
			token.CharInd,
			"" )
	} else {
		return fmt.Sprintf(
			"<ty-%s ind-%d val-%s>",
			TypeDesc( token.Ltype ),
			token.CharInd,
			string( token.Word[ 0 ] ) )
	}
}

func TypeDesc( enumVal int ) string {
	switch enumVal {
		case T_L_PAREN :
			return "L-paren"
		case T_R_PAREN :
			return "R-paren"
		case T_PLUS :
			return "Plus"
		case T_MINUS :
			return "Minus"
		case T_MULTIPLY :
			return "Times"
		case T_DIVIDE :
			return "Divide"
		case T_NUMBER :
			return "Int"
		case T_BLANK :
			return "Space"
		case T_END_LINE :
			return "Eol"
		case T_END_FILE :
			return "Eof"
		case T_START_FILE :
			return "Bof"
		case T_UNKNOWN :
			return "Unknown"
		default :
			return "Uhandled"
	}
}



















