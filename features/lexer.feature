
Feature: calculate integer math
	integer values, pedmas operations

	Scenario: nothing
		When no input
		Then no output

	Scenario: just operators
		When input is just operators "*+/-()"
		Then output is just operators "times plus div minus lparen rparen"

	Scenario: bare number
		When input is numeral 35
		Then output is numeral 35

	Scenario: single line expression
		When input is expression "35 -   8/4+(*77777)"
		Then output is expression "number35 space1 minus space3 number8 div number4 plus lparen times number77777 rparen"



















