
package ws.nzen.format.calc;

fun main() {
	println( "** blank" )
	var expression = ""
	showLexed( expression )
	println( "** operators only" )
	expression = "+/--()"
	showLexed( expression )
	println( "** single number" )
	expression = "12"
	showLexed( expression )
	println( "** spaces only" )
	expression = "  "
	showLexed( expression )
	println( "** no float" )
	expression = "8.3"
	showLexed( expression )
	println( "** simple expresison" )
	expression = "1 + 2"
	showLexed( expression )
	println( "** complex expression" )
	expression = "1 + 23 -45 +(6)"
	showLexed( expression )
}


fun showLexed( expression : String ) {
	println( expression )
	val lexed = lex( expression )
	for ( lexeme : Lexeme in lexed ) {
		if ( lexeme.lType == LexType.WSPACE ) {
		println( lexeme.lType.name +" - \""+ lexeme.rune +"\" @ "+ lexeme.startIndex )
		} else {
		println( lexeme.lType.name +" - "+ lexeme.rune +" @ "+ lexeme.startIndex )
		}
	}
}


fun lex( original : String ) : List<Lexeme> {
	var result = mutableListOf<Lexeme>()
	var ind = 0
	while ( ind < original.length ) {
		var currC = original.get( ind )
		if ( currC.equals( '+' ) ) {
			result.add( Lexeme(
					LexType.OP_PLUS,
					currC.toString(),
					ind
			) )
			ind += 1
			continue
		} else if ( currC.equals( '-' ) ) {
			result.add( Lexeme(
					LexType.OP_MINUS,
					currC.toString(),
					ind
			) )
			ind += 1
			continue
		} else if ( currC.equals( '*' ) ) {
			result.add( Lexeme(
					LexType.OP_TIMES,
					currC.toString(),
					ind
			) )
			ind += 1
			continue
		} else if ( currC.equals( '/' ) ) {
			result.add( Lexeme(
					LexType.OP_DIVIDE,
					currC.toString(),
					ind
			) )
			ind += 1
			continue
		} else if ( currC.equals( '(' ) ) {
			result.add( Lexeme(
					LexType.OP_L_PAREN,
					currC.toString(),
					ind
			) )
			ind += 1
			continue
		} else if ( currC.equals( ')' ) ) {
			result.add( Lexeme(
					LexType.OP_R_PAREN,
					currC.toString(),
					ind
			) )
			ind += 1
			continue
		}
		// NOTE multiline
		if ( currC.isWhitespace() ) {
			var endInd = ind
			while ( endInd < original.length ) {
				var cursorC = original.get( endInd )
				if ( cursorC.isWhitespace() ) {
					endInd += 1
				} else {
					break
				}
			}
			result.add( Lexeme(
					LexType.WSPACE,
					original.substring( ind, endInd ),
					ind
			) )
			ind = endInd
			continue
		} else if ( currC.isDigit() ) {
			var endInd = ind
			while ( endInd < original.length ) {
				var cursorC = original.get( endInd )
				if ( cursorC.isDigit() ) {
					endInd += 1
				} else {
					break
				}
			}
			result.add( Lexeme(
					LexType.INT,
					original.substring( ind, endInd ),
					ind
			) )
			ind = endInd
			continue
		}
		/* IMPROVE just one for loop instead with a function reference
		FIX disabled for commit  ( () ) {
		
		}
		*/
		else {
			result.add( Lexeme(
					LexType.UNKNOWN,
					currC.toString(),
					ind
			) )
			ind += 1
			continue
		}
	}
	return result
}

class Lexeme(
		val lType: LexType,
		val rune: String, 
		val startIndex: Int ) {
}

enum class LexType {
	INT,
	WSPACE,
	OP_PLUS,
	OP_MINUS,
	OP_TIMES,
	OP_DIVIDE,
	OP_L_PAREN,
	OP_R_PAREN,
	END_LINE,
	UNKNOWN
}

/*
running by hand

* Lexeme.kt is in the root *
 /d/Programs64b/base/kotlin/v1_3_50/bin/kotlinc.bat -d bin -cp . Lexer.kt
 /d/Programs64b/base/kotlin/v1_3_50/bin/kotlin.bat -cp bin ws.nzen.format.calc.LexerKt
*/



































