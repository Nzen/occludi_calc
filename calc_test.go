
package calc // preferably nzen.ws/format/calc

import (
	"flag"
	"fmt"
	"os"
	"strconv"
	"strings"
	"testing"
	"unicode/utf8"
	"github.com/DATA-DOG/godog"
	"github.com/DATA-DOG/godog/colors"
)

var opt = godog.Options{
	Output: colors.Colored(os.Stdout),
	Format: "progress", // can define default values
}

func init() {
	godog.BindFlags("godog.", flag.CommandLine, &opt)
}

func TestMain(m *testing.M) {
	flag.Parse()
	opt.Paths = flag.Args()

	status := godog.RunWithOptions("godogs", func(s *godog.Suite) {
		FeatureContext(s)
	}, godog.Options {
		Format : "pretty",
	})

	if st := m.Run(); st > status {
		status = st
	}
	os.Exit(status)
}

// the input saved as prep for each scenario
var inputStr string

func FeatureContext( runner *godog.Suite ) {
	// Scenario: nothing
	runner.Step( `^no input$`, inputIsNothing )
	runner.Step( `^no output$`, inputIsNothing )
	// Scenario: just operators
	runner.Step( `^input is just operators "([^"]*)"$`, inputIsJustOperators )
	runner.Step( `^output is just operators "([^"]*)"$`, outputIsJustOperators )
	// Scenario: bare number
	runner.Step( `^input is numeral (\d+)$`, inputIsSingleNumeral ) // NOTE regex
	runner.Step( `^output is numeral (\d+)$`, outputIsSingleNumeral )
	// Scenario: single line expression
	runner.Step( `^input is expression "([^"]*)"$`, inputIsSingleLine ) // NOTE regex
	runner.Step( `^output is expression "([^"]*)"$`, outputIsSingleLine )

	//
	runner.BeforeScenario(
		func( interface{} ) {
			inputStr = ""
		} )
}

func inputIsNothing () error {
	inputStr = ""
	return nil
}

func outputIsNothing () error {
	understand := Lexer{}
	understand.Lex( inputStr )
	actual := understand.NextToken()
	expected := Lexeme{ T_END_FILE, 0, 0, []rune{ R_eof } }
	return compareLexeme( expected, actual, "output single numeral" )
}

func inputIsJustOperators( punctuation string ) error {
	// Improve if punct is nil or empty or contains non operators retur err or change it
	inputStr = punctuation
	return nil
}

func outputIsJustOperators( expectedAsNames string ) error {
	opNames := strings.Split( expectedAsNames, " " )
	understand := Lexer{}
	understand.Lex( inputStr )
	var problem error
	for byteInd, name := range opNames {
		actual := understand.NextToken()
		expected := operatorNameToLexeme( name, 0, byteInd )
		problem = compareLexeme( expected, actual, "output just operators, "+ name )
			// 
		if problem != nil {
			return problem
		} else {
			fmt.Println( actual )
		}
	}
	return nil
}

func inputIsSingleNumeral( amount int ) error {
	inputStr = strconv.Itoa( amount )
	return nil
}

func outputIsSingleNumeral( amount int ) error {
	understand := Lexer{}
	understand.Lex( inputStr )
	actual := understand.NextToken()
	expected := lexemeOf( amount, 0, 0 )
	return compareLexeme( expected, actual, "output single numeral" )
}

func inputIsSingleLine( punctuation string ) error {
	inputStr = punctuation
	return nil
}

func outputIsSingleLine( expectedAsNames string ) error {
	opNames := strings.Split( expectedAsNames, " " )
	understand := Lexer{}
	understand.Lex( inputStr )
	var problem error
	for byteInd, name := range opNames {
		actual := understand.NextToken()
		expected := operatorNameToLexeme( name, 0, byteInd )
		problem = compareLexeme( expected, actual, "output single line, "+ name )
			// 
		if problem != nil {
			return problem
		} else {
			fmt.Println( actual )
		}
	}
	return nil
}

func compareLexeme( expected Lexeme, actual Lexeme, message string ) error {
	// NOTE apparently Lexemes can never be nil without some conversion feature
	if expected.Ltype != actual.Ltype {
		return fmt.Errorf( "%s type mismatch e-%s a-%s",
			message, TypeDesc( expected.Ltype ), TypeDesc( actual.Ltype ) )
	}
	if expected.LineInd != actual.LineInd {
		return fmt.Errorf( "%s line mismatch e-%d a-%d",
			message, expected.LineInd, actual.LineInd )
	}
	if expected.CharInd != actual.CharInd {
		return fmt.Errorf( "%s cursor mismatch e-%d a-%d",
			message, expected.CharInd, actual.CharInd )
	}
	if len( expected.Word ) != len( actual.Word ) {
		return fmt.Errorf( "%s word-len mismatch e-%d a-%d",
			message, len( expected.Word ), len( actual.Word ) )
	}
	for ind, literal := range actual.Word {
		if literal != expected.Word[ ind ] {
			return fmt.Errorf( "%s rune mismatch e-%d a-%d at a-%d",
				message, literal, expected.Word[ ind ], ind )
			break
		}
	}
	return nil
}

func lexemeOf( amount, line, charInd int ) Lexeme {
	amountStr := strconv.Itoa( amount )
	itoa := make( []rune, utf8.RuneCountInString( amountStr ) )
	ind := 0
	for _, literal := range amountStr {
		if ind >= len( itoa ) {
			fmt.Errorf( "test writer, index out of bounds %d", ind )
			break
		} else {
			itoa[ ind ] = literal
			ind += 1
		}
	}
	return Lexeme { T_NUMBER, line, charInd, itoa }
}

func operatorNameToLexeme( name string, line int, charInd int ) Lexeme {
	switch name {
		case "lparen" :
			return Lexeme{ T_L_PAREN, line, charInd, []rune{ rune( '(' ) } }
		// -- here
		case "rparen" :
			return Lexeme{ T_R_PAREN, line, charInd, []rune{ rune( ')' ) } }
		case "div" :
			return Lexeme{ T_DIVIDE, line, charInd, []rune{ rune( '/' ) } }
		case "times" :
			return Lexeme{ T_MULTIPLY, line, charInd, []rune{ rune( '*' ) } }
		case "plus" :
			return Lexeme{ T_PLUS, line, charInd, []rune{ rune( '+' ) } }
		case "minus" :
			return Lexeme{ T_MINUS, line, charInd, []rune{ rune( '-' ) } }
		case strings.Contains( name, "space" ) :
			// format is [[ space ]][[ number ]], number is an int literal of the spaces
			// ASK but should I gen or make the real thing?
			blanks := make( []rune, utf8.RuneCountInString(
					strconv.Atoi( name[ len("space"): ] ) )
			for ind := 0; ind < len( blanks ); ind += 1 {
				
			}
			return Lexeme{ T_BLANK, line, charInd, []rune{ rune( '-' ) } }
		default :
			// hopefully unnecessary
			return Lexeme{ T_END_FILE, line, charInd, []rune{ rune( 'x' ) } }
	}
}





























